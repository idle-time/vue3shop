import router from '@/router'
import store from '@/store'
import { watchEffect } from 'vue'
// 面包屑 —— 分类模式
export const categoryMode = breadItems => {
  watchEffect(() => {
    const route = router.currentRoute.value
    const list = store.state.category.categoryList
    outer: for (const top of list) {
      if (top.id === route.params.id) {
        // 设置一级类目
        breadItems.value[1] = { name: top.name }
        break
      }
      if (top.children) {
        for (const sub of top.children) {
          if (sub.id === route.params.id) {
            // 设置一、二级类目
            breadItems.value[1] = {
              name: top.name,
              path: `/category/${top.id}`
            }
            breadItems.value[2] = { name: sub.name }
            break outer
          }
        }
      }
    }
  })
}
// 排序，因为接口有问题
const breadSort = arr => {
  const result = []
  arr.forEach(item => {
    if (!item.parent) result.push([item])
  })
  result.forEach(resultItem => {
    arr.forEach(arrItem => {
      if (resultItem[0].id === arrItem.parent?.id) {
        resultItem.push(arrItem)
      }
    })
  })
  return result.flat()
}
// 面包屑 —— 商品模式
export const productMode = (breadItems, data) => {
  const categories = breadSort(data.categories)
  breadItems.value[1] = {
    name: categories[0].name,
    path: `/category/${categories[0].id}`
  }
  breadItems.value[2] = {
    name: categories[1].name,
    path: `/category/sub/${categories[1].id}`
  }
  breadItems.value[3] = { name: data.name }
}
