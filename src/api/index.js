import request from '@/common/network/request'

// 获取首页头部分类导航数据
export const homeCategoryHeader = params => {
  return request({
    url: '/home/category/head',
    method: 'GET',
    params
  })
}
// 获取首页品牌分类数据
export const homeBrand = params => {
  return request({
    url: '/home/brand',
    method: 'GET',
    params: {
      limit: 6,
      ...params
    }
  })
}
// 获取轮播图数据
export const homeBanner = () => {
  return request({
    url: '/home/banner',
    method: 'GET'
  })
}
// 获取新鲜好物数据
export const homeNew = () => {
  return request({
    url: 'home/new',
    method: 'GET'
  })
}
// 获取人气推荐
export const homeHot = () => {
  return request({
    url: 'home/hot',
    method: 'GET'
  })
}
// 获取商品数据
export const homeGoods = () => {
  return request({
    url: 'home/goods',
    method: 'GET'
  })
}
// 获取最新专题
export const homeSpecial = () => {
  return request({
    url: 'home/special',
    method: 'GET'
  })
}
// 获取一级分类数据
export const apiCategory = (params, options) => {
  return request({
    ...options,
    url: '/category',
    method: 'GET',
    params
  })
}
// 获取二级分类筛选区数据
export const apiCategorySubFilter = (params, options) => {
  return request({
    ...options,
    url: '/category/sub/filter',
    method: 'GET',
    params
  })
}
// 获取二级分类商品区数据
export const apiCategoryGoodsTemporary = (data, options) => {
  return request({
    ...options,
    url: '/category/goods/temporary',
    method: 'POST',
    data
  })
}
// 获取商品详情
export const apiGoods = (params, options) => {
  return request({
    ...options,
    url: '/goods',
    method: 'GET',
    params
  })
}
