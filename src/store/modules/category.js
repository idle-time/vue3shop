import { homeCategoryHeader } from '@/api'
export default {
  namespaced: true,
  state () {
    return {
      // 分类信息
      categoryList: [
        { name: '居家' },
        { name: '美食' },
        { name: '服饰' },
        { name: '母婴' },
        { name: '个护' },
        { name: '严选' },
        { name: '数码' },
        { name: '运动' },
        { name: '杂货' }
      ]
    }
  },
  mutations: {
    setcategoryList (state, payload) {
      state.categoryList = payload
    }
  },
  actions: {
    async getcategoryList ({ commit, state }) {
      // 获取分类数据
      const { result } = await homeCategoryHeader().catch(() => {
        return { result: state.categoryList }
      })
      // 修改分类数据
      commit('setcategoryList', result)
    }
  }
}
