export default {
  namespaced: true,
  state () {
    return {
      // 用户信息
      userInfo: {}
    }
  },
  mutations: {
    setUserInfo (state, payload) {
      state.userInfo = payload
    }
  }
}
