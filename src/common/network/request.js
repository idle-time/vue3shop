import axios from 'axios'
import store from '@/store'
import router from '@/router'
import qs from 'qs'

// 基地址
export const baseURL = process.env.VUE_APP_BASE_API

// 创建axios实例
const request = axios.create({
  baseURL,
  timeout: 10000
})
// 请求拦截器
request.interceptors.request.use(
  config => {
    const { token } = store.state.user.userInfo
    // 每次请求加上token
    if (token) config.headers.Authorization = `Bearer ${token}`
    // 对post请求进行处理
    if (config.isForm && config.method === 'post') {
      config.data = qs.stringify(config.data)
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)
// 响应拦截器
request.interceptors.response.use(
  response => {
    return response.data
  },
  error => {
    if (error.response && error.response.status === 401) {
      store.commit('user/setUser', {})
      const fullPath = router.currentRoute.value.fullPath
      router.push('/login?redirectFrom=' + fullPath)
    }
    return Promise.reject(error)
  }
)

export default request
