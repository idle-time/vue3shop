// vue插件
import directive from './directive'
const components = require.context('./components', false, /\.vue$/)

export default {
  install (app) {
    components.keys().forEach(k => {
      const component = components(k).default
      app.component(component.name, component)
    })
    Object.keys(directive).forEach(k => app.directive(k, directive[k]))
  }
}
