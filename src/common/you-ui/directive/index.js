// import { watch } from 'vue'
import defultImage from '@/assets/images/200.png'

export default {
  lazyData: {
    mounted (el, { dir, value, arg = 300, instance }) {
      function handleLazyLoad () {
        const top = el.getBoundingClientRect().top
        const clientHeight = document.documentElement.clientHeight
        const isToLoad = top <= clientHeight + +arg
        if (isToLoad) {
          value && value()
          window.removeEventListener('scroll', handleLazyLoad)
          window.removeEventListener('resize', handleLazyLoad)
        }
      }
      // 页面resize时懒加载
      // 1.解决方案一：使用resize事件监听可视区域
      window.addEventListener('resize', handleLazyLoad)
      /**
       * 2.解决方案二：使用watch监听可视区域
       * 在main.js中绑定一个全局响应式属性clientHeight（可在main.js中查看），
       * 通过instance.clientHeight获取(instance为当前组件实例)
       */
      // stop方法为主动停止监听方法，详情见：https://v3.cn.vuejs.org/guide/reactivity-computed-watchers.html#%E5%81%9C%E6%AD%A2%E4%BE%A6%E5%90%AC

      // //  方案二代码
      // const stop = watch(instance.clientHeight, nV => {
      //   console.log(nV)
      //   const isToLoad = el.getBoundingClientRect().top - nV <= 300
      //   if (isToLoad) {
      //     stop()
      //     value && value()
      //     window.removeEventListener('scroll', onScroll)
      //   }
      // })

      // 页面滚动时懒加载
      window.addEventListener('scroll', handleLazyLoad)
      // 进入页面时立即执行一次，因为可能一开始就出现在可视范围内
      handleLazyLoad()
      // 因为有多个懒加载函数，所以要放在数组里，不然会覆盖
      dir.handleLazyLoad ??= []
      dir.handleLazyLoad.push(handleLazyLoad)
    },
    unmounted (el, { dir }) {
      // 组件卸载时移除事件监听，例如进入其他页面时
      const currHandle = dir.handleLazyLoad.shift()
      window.removeEventListener('scroll', currHandle)
      window.removeEventListener('resize', currHandle)
    }
  },
  lazyImg: {
    mounted (el, { value }) {
      // 创建监听实例
      const observer = new IntersectionObserver(([{ isIntersecting }]) => {
        if (isIntersecting) {
          // 取消监听
          observer.unobserve(el)
          // 加载图片
          el.src = value
          // 图片加载错误时，使用默认图片
          el.onerror = () => {
            el.src = defultImage
          }
        }
      })
      // 监听元素
      observer.observe(el)
    }
  },
  visibility (el, { value }) {
    if (value) el.style.visibility = 'visible'
    else el.style.visibility = 'hidden'
  }
}
