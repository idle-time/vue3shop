export const scrollToMemory = () => {
  let { scroll } = history.state
  if (navigator.userAgent.includes('Firefox')) {
    scroll = JSON.parse(sessionStorage.getItem('scroll'))
  }
  setTimeout(() => {
    window.scrollTo(scroll)
  })
}
