// 返回数据类型
export const dataType = data => {
  return Object.prototype.toString.call(data).slice(8, -1)
}
export const isObject = data => {
  return dataType(data) === 'Object'
}
