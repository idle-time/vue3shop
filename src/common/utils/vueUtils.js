import { unref } from 'vue'
import { isObject } from './function'

// v-if
export const vIf = (condition, el, elseEl) => {
  if (unref(condition)) return el
  else return (elseEl ??= '')
}
// v-for
export const vFor = (items, callback) => {
  items = unref(items)
  if (typeof items === 'number') {
    const length = items
    items = []
    // eslint-disable-next-line
    for (let i = 1; items.push(i++) < length; ) {}
  } else if (isObject(items)) {
    items = Object.entries(items)
  }
  return items.map(callback)
}
