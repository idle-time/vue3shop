import { createRouter, createWebHashHistory } from 'vue-router'

// 定义路由信息
const routes = [
  {
    path: '/',
    redirect: '/home',
    meta: {
      title: '小兔鲜儿 - 新鲜 惠民 快捷！',
      breadcrumb: [{ name: '首页', path: '/' }]
    },
    component: () => import('@/views/layout.vue'),
    children: [
      {
        alias: '/',
        path: 'home',
        name: 'home',
        component: () => import('@/views/home/home.vue')
      },
      {
        path: 'category/:id',
        name: 'category',
        meta: {
          title: '一级分类',
          breadcrumb: [{ name: '一级分类' }]
        },
        component: () => import('@/views/category/category.vue')
      },
      {
        path: 'category/sub/:id',
        name: 'category-sub',
        meta: {
          title: '二级分类',
          breadcrumb: [{ name: '一级分类' }, { name: '二级分类' }]
        },
        component: () => import('@/views/category/subCategory.vue')
      },
      {
        path: '/product/:id',
        name: 'product',
        meta: {
          title: '商品详情',
          breadcrumb: [
            { name: '一级分类' },
            { name: '二级分类' },
            { name: '商品详情' }
          ]
        },
        component: () => import('@/views/product/product.vue')
      }
    ]
  }
]

// 滚动行为
const scrollBehavior = (to, from, savedPosition) => {
  // return desired position
  // if (savedPosition) {
  //   return savedPosition
  // } else {
  return {
    top: 0,
    left: 0
  }
  // }
}

// 创建路由
const router = createRouter({
  // 使用hash路由模式
  history: createWebHashHistory(),
  routes,
  scrollBehavior
})

// 全局导航守卫
router.beforeEach((to, from) => {
  // to and from are Route Object,next() must be called to resolve the hook
  if (to.meta.title) document.title = to.meta.title
  else document.title = '小兔鲜儿 - 新鲜 惠民 快捷！'
})
export default router
