import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'normalize.css' // 标准化css npm i normalize.css
import './assets/styles/scss/common.scss' // 自己的common样式
import youUI from '@/common/you-ui' // 自己写的插件
const app = createApp(App)

// // 数据懒加载方案二代码
// const clientHeight = ref(document.documentElement.clientHeight)
// window.addEventListener('resize', () => {
//   clientHeight.value = document.documentElement.clientHeight
// })
// // 用到了vue3的全局属性，相当于vue2的Vue.prototype.$xxx=xxx  https://v3.cn.vuejs.org/api/application-config.html#globalproperties
// app.config.globalProperties.clientHeight = clientHeight

// 页面刷新前保存当前位置
if (navigator.userAgent.includes('Firefox')) {
  window.onbeforeunload = () => {
    sessionStorage.setItem(
      'scroll',
      JSON.stringify({
        top: window.scrollY,
        left: window.scrollX
      })
    )
  }
}

app
  .use(store)
  .use(router)
  .use(youUI)
  .mount('#app')
