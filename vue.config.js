const { defineConfig } = require('@vue/cli-service')
const path = require('path')
module.exports = defineConfig({
  transpileDependencies: true,
  // 配置网页标题
  chainWebpack: config => {
    config.plugin('html').tap(args => {
      args[0].title = '小兔鲜儿 - 新鲜 惠民 快捷！'
      return args
    })
  },
  // 开发服务器配置
  devServer: {
    proxy: {
      '/': {
        target: 'http://pcapi-xiaotuxian-front-devtest.itheima.net',
        changeOrigin: true,
        ws: false
      }
    }
  },
  // 使用vue-cli-plugin-style-resources-loader配置scss文件全局注入 https://github.com/robinvdvleuten/vuex-persistedstate
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        path.resolve(__dirname, './src/assets/styles/scss/variables.scss'),
        path.resolve(__dirname, './src/assets/styles/scss/mixins.scss')
      ]
    }
  }
})
